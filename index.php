<?php

if(isset($_POST) && !empty($_POST)){
	require "Empresa.php";
	$empresa = new Empresa();

	if($empresa->cadastrar_empresa($_POST)){		
		$parametros = [];
		$parametros['nome'] = $empresa->getNome();
		$parametros['cnpj'] = "";
		$parametros['faturamento'] = "";
		include "pagina_sucesso.php";		
	}else{
		include "pagina_erro.php";
	}
}

if(empty($_POST))
	include "form_empresa.html";

?>