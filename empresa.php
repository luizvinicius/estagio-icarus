<?php
class Empresa{
	
	private $nome;
	private $cnpj;
	private $faturamento;
	private $tipo_empresa;
	private $opcoes_tipo_empresa = [
		 0 => 'Pequena',
		 1 => 'Média',
		 2 => 'Grande'
	];
		
		
	public function getNome(){
		return $this->nome;
	}
	
	public function setNome($nome_set){
		$this->nome = $nome_set;
	}
	
	public function cadastrar_empresa($POST){
		
		$nome = isset($POST['nome'])?$POST['nome']:'';
		$cnpj = isset($POST['cnpj'])?'':$POST['cnpj'];
		$this->setNome($nome);
		
		if(empty($nome)){
			return false;
		}
		
		return true;
	}
}