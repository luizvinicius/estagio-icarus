=========================================================

Olá candidato,

Apresentamos a você esse pequeno projeto que baseia-se em um formulário HTML,
e um script em PHP com o intuito de simular um cadastro de uma Empresa no sistema.
Elaboramos algumas tarefas simples para avaliar o seu conhecimento HTML, PHP e O.O.

No contexto do cadastro temos o modelo chamado Empresa que possui três atributos
(nome, cnpj e faturamento). Existem três telas, sendo a primeira o formulário,
uma página de sucesso do cadastro e a outra de erro. Ao preencher e enviar o formulário
o usuário é encaminhado para uma tela com as informações enviadas e a mensagem de sucesso.
É obrigatório o preenchimento do campo "nome", caso contrário o usuário é redireciono
para uma página de erro.

Suas tarefas são implementar:

01 Obrigatoriedade no preenchimento do CNPJ e Faturamento da empresa, não deve ser permitido o cadastro dela caso
esses campos estejam vazios.

02 Ao ser redirecionado para a página de sucesso, na tabela que contém as informações da empresa,
dever ser completada com as que faltam (CNPJ e Faturamento).

03 Na tela de sucesso, adicionar uma coluna que informa se a empresa cadastrada é pequena, média ou grande.

A referência é:

Pequena: faturamento menor ou igual a 50.000.

Média: faturamento maior que 50.000 e menor ou igual a 250.000.

Grande: faturamento maior que 500.000.

=========================================================