<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/gif" href="http://vocesaude.icarusocupacional.com.br/assets//images/main/favicon.png" class="__web-inspector-hide-shortcut__">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Empresa</title>
  </head>
  <body>
  
  <div class="container">
	<img src="https://icarusocupacional.com.br/templates/images/logo.png" class="rounded mx-auto d-block img-fluid" alt="..." />
	<div class="alert alert-success" role="alert">
		Empresa cadastrada com sucesso!
	</div>
	<table class="table">
	  <thead>
		<tr>
		  <th scope="col">#</th>
		  <th scope="col">Nome</th>
		  <th scope="col">CNPJ</th>
		  <th scope="col">Faturamento</th>
		</tr>
	  </thead>
	  <tbody>
		<tr>
		  <th scope="row">1</th>
		  <td><?= $parametros['nome']?></td>
		  <td><?= $parametros['cnpj']?></td>
		  <td><?= $parametros['faturamento']?></td>
		</tr>
	  </tbody>
	</table>
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
  </body>
</html>